from sklearn.pipeline import Pipeline
from Functions.Selector import ColumnAdder
from Functions import preprocessing
from Functions import feature
import pandas as pd
from apyori import apriori


# Define pipelines #


def text_preprocess(string, except_list):
    string = preprocessing.remove_accents(string)
    string = preprocessing.lower_string(string, except_list)
    string = preprocessing.remove_special_char(string)
    string = preprocessing.strip_string(string)
    string = preprocessing.remove_punctuation(string)
    string = preprocessing.remove_stop_words(string)
    string = preprocessing.lemmatize(string)
    return preprocessing.stemming(string)



normalize_text = Pipeline([
    ('removeAccents', preprocessing.RemoveAccents()),
    ('lowerString', preprocessing.LowerString()),
    ('removeSpecialChar', preprocessing.RemoveSpecialChar()),
    ('stripString', preprocessing.StripString()),
    ('cleanPunctuation', preprocessing.CleanPunctuation()),
    ('filterStopWords', preprocessing.RemoveStopWords()),
    ('lemString', preprocessing.LemmatizeString()),
    ('stemString', preprocessing.StemString())
])


data_enrichment = Pipeline([
    ('addData', ColumnAdder)
])


# Model #


def association_vector_query_title(data):
    title_matrix = feature.count_term_matrix(data[data['relevance'] >= 2.50]['product_title'])
    search_term_matrix = feature.count_term_matrix(data[data['relevance'] >= 2.50]['search_term'])
    matrix = title_matrix.merge(search_term_matrix, how='inner').values.tolist()
    ass_rules = list(apriori(matrix, min_support=0.2, min_confidence=0.5, min_lift=3, min_length=3))
    return ass_rules


def counts_binary_model(data, ngram_low=1, ngram_hi=2):
    # Compute count vectors for the matrices #
    product_title_matrix = feature.binary_occurrence_matrix(data['product_title'].tolist(), ngram_low, ngram_hi)
    product_title_matrix['__temptipo__'] = 'title'
    search_term_matrix = feature.binary_occurrence_matrix(data['search_term'].tolist(), ngram_low, ngram_hi)
    search_term_matrix['__temptipo__'] = 'query'
    brand_matrix = feature.binary_occurrence_matrix(data['brand'].tolist(), ngram_low, ngram_hi)
    brand_matrix['__temptipo__'] = 'brand'
    material_matrix = feature.binary_occurrence_matrix(data['material'].tolist(), ngram_low, ngram_hi)
    material_matrix['__temptipo__'] = 'material'
    product_des_matrix = feature.binary_occurrence_matrix(data['product_description'].tolist(), ngram_low, ngram_hi)
    product_des_matrix['__temptipo__'] = 'description'
    # Combine all matrices #
    combine = [product_title_matrix,
               search_term_matrix,
               brand_matrix,
               material_matrix,
               product_des_matrix]
    result = pd.concat(combine)
    result.reset_index(drop=True, inplace=True)
    result.fillna(0, inplace=True)
    # Build different matrices based on temporary column #
    product_title_matrix = result[result['__temptipo__'] == 'title']
    search_term_matrix = result[result['__temptipo__'] == 'query']
    brand_matrix = result[result['__temptipo__'] == 'brand']
    material_matrix = result[result['__temptipo__'] == 'material']
    product_des_matrix = result[result['__temptipo__'] == 'description']
    # Remove temporary column #
    product_title_matrix.pop('__temptipo__')
    search_term_matrix.pop('__temptipo__')
    brand_matrix.pop('__temptipo__')
    material_matrix.pop('__temptipo__')
    product_des_matrix.pop('__temptipo__')
    return product_title_matrix, search_term_matrix, brand_matrix, material_matrix, product_des_matrix



def vector_model(data, ngram_low=1, ngram_hi=2):
    # Compute tfidf vectors for the matrices #
    product_title_matrix = feature.tfidf_matrix(data['product_title'].tolist(), ngram_low, ngram_hi)
    product_title_matrix['__temptipo__'] = 'title'
    search_term_matrix = feature.tfidf_matrix(data['search_term'].tolist(), ngram_low, ngram_hi)
    search_term_matrix['__temptipo__'] = 'query'
    brand_matrix = feature.tfidf_matrix(data['brand'].tolist(), ngram_low, ngram_hi)
    brand_matrix['__temptipo__'] = 'brand'
    material_matrix = feature.tfidf_matrix(data['material'].tolist(), ngram_low, ngram_hi)
    material_matrix['__temptipo__'] = 'material'
    product_des_matrix = feature.tfidf_matrix(data['product_description'].tolist(), ngram_low, ngram_hi)
    product_des_matrix['__temptipo__'] = 'description'
    # Combine all matrices #
    combine = [product_title_matrix,
               search_term_matrix,
               brand_matrix,
               material_matrix,
               product_des_matrix]
    result = pd.concat(combine)
    result.reset_index(drop=True, inplace=True)
    result.fillna(0, inplace=True)
    # Build different matrices based on temporary column #
    product_title_matrix = result[result['__temptipo__'] == 'title']
    search_term_matrix = result[result['__temptipo__'] == 'query']
    brand_matrix = result[result['__temptipo__'] == 'brand']
    material_matrix = result[result['__temptipo__'] == 'material']
    product_des_matrix = result[result['__temptipo__'] == 'description']
    # Remove temporary column #
    product_title_matrix.pop('__temptipo__')
    search_term_matrix.pop('__temptipo__')
    brand_matrix.pop('__temptipo__')
    material_matrix.pop('__temptipo__')
    product_des_matrix.pop('__temptipo__')
    return product_title_matrix, search_term_matrix, brand_matrix, material_matrix, product_des_matrix


# Features #


def base_feature_extraction(data):
    features = pd.DataFrame()
    features['query_in_title'] = feature.is_query_in_title(data)
    features['query_in_description'] = feature.is_query_in_description(data)
    features['brand_in_query'] = feature.is_brand_in_query(data)
    features['material_in_query'] = feature.is_material_in_query(data)
    features['common_word_query_desc'] = feature.get_common_words_query_description(data)
    features['common_word_query_title'] = feature.get_common_words_query_title(data)
    features['num_of_word_query'] = feature.get_num_of_words_query(data)
    features['num_of_word_desc'] = feature.get_num_of_words_desc(data)
    features['num_of_word_title'] = feature.get_num_of_words_title(data)
    features['smallwin_query_title'] = feature.get_smallest_window_query_title(data)
    features['smallwin_query_desc'] = feature.get_smallest_window_query_description(data)
    return features


def advanced_feature_extraction(doc, query):
    sim = feature.get_similarity_from_term_matrix(doc, query)
    return sim.diagonal()
