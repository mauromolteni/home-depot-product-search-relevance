import os

PROJ_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
DATASET_PATH = os.path.join(PROJ_DIR_PATH,'Data')

DEBUG = False
VISUALIZATION = True

STATISTICAL_MODELS = {
    '0': 'Linear Regression',
    '1': 'Random Forest',
    '2': 'Ridge Regression',
    '3': 'Gradient Boosting',
    '4': 'PCA Boosting',
    '5': 'Adaptive Boosting'
}
