"""
    This file includes the procedures for the creation of the datasets, as a starting point for training.
    Translation of the text into numerical features.
    Creation of different datasets because we evaluate different language representation models.
"""

from Nlp import preparation
from Nlp import processing
from Nlp import featextraction
from Functions import utils
import definitions


#############
# Load data #
#############


[train_dataset, attributes, products] = preparation.load_dataset()
brand, material = preparation.get_attributes(attributes)
brand['brand'] = preparation.clean_dataset(brand[['brand']])
brand_list = utils.unique_from_list(list(brand['brand'].values))
maximum_length_of_brand = max(brand['brand'].str.split().apply(len).value_counts().index)
material['material'] = preparation.clean_dataset(material[['material']])
del attributes


if definitions.DEBUG:
    train_dataset = train_dataset.head(100)


################
# Process data #
################


train_dataset = preparation.enrich_data(train_dataset, brand, material, products)
train_dataset = processing.process_dataset(train_dataset)

train_dataset.to_csv('clean_dataset.csv', index=False)

print("End of processing.")


########################
# Feature  Engineering #
########################


models = {
    "first": featextraction.feat_eng_first_model,
    "second": featextraction.feat_eng_second_model,
    "third": featextraction.feat_eng_third_model,
    "fourth": featextraction.feat_eng_fourth_model,
    "fifth": featextraction.feat_eng_fifth_model,
    "sixth": featextraction.feat_eng_sixth_model,
    "seventh": featextraction.feat_eng_seventh_model,
    "eighth": featextraction.feat_eng_eighth_model
}


for m in models:
    features = models[m](train_dataset)
    filename = definitions.DATASET_PATH+'/nlp_model_'+m+'.csv'
    features.to_csv(filename, index=False)


print("All models have been generated.")