import os
import pandas as pd
import matplotlib.pyplot as plt
import definitions
import re
from Nlp import learningmodels
from definitions import DATASET_PATH
from definitions import STATISTICAL_MODELS
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


# Load models #


models = []
model_names = []
os.chdir(DATASET_PATH)
for file in os.listdir(DATASET_PATH):
    filename = os.fsdecode(file)
    if filename.startswith('nlp'):
        models.append(pd.read_csv(filename))
        model_names.append(filename)

model_names = list(map(lambda x: re.search('nlp_(.+?).csv', x).group(1), model_names))


# Visualize some statistics #


def visualize_features(features):
    if definitions.VISUALIZATION:
        ax = features.plot.scatter(x='smallwin_query_title', y = 'relevance', c = 'DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Smallest window between query and product title")

        ax = features.plot.scatter(x = 'smallwin_query_desc', y='relevance', c = 'DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Smallest window between query and product description")

        ax = features.plot.scatter(x='similarity_query_title', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Cosine similarity between query and product title")

        ax = features.plot.scatter(x='similarity_query_brand', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Cosine similarity between query and brand")

        ax = features.plot.scatter(x='similarity_query_material', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Cosine similarity between query and material")

        ax = features.plot.scatter(x='similarity_query_description', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Cosine similarity between query and product description")

        ax = features.plot.scatter(x='query_in_title', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Query in product title")

        ax = features.plot.scatter(x='query_in_description', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Query in product description")

        ax = features.plot.scatter(x='brand_in_query', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Brand in query")

        ax = features.plot.scatter(x='material_in_query', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Material in query")

        ax = features.plot.scatter(x='common_word_query_desc', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Common words between query and product desc")

        ax = features.plot.scatter(x='common_word_query_title', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Common words between query and product title")

        ax = features.plot.scatter(x='num_of_word_query', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Number of words in query")

        ax = features.plot.scatter(x='num_of_word_desc', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Number of words in product description")

        ax = features.plot.scatter(x='num_of_word_title', y='relevance', c='DarkBlue')
        ax.set_ylabel("Relevance")
        ax.set_xlabel("Number of words in product title")

        plt.show()


# Evaluate different statistical learning techniques on the different models #


print("Start statistical learning")
idx = 0
results = []
for model in models:
    print("Model " + model_names[idx])
    model.drop(['product_uid'], axis=1, inplace=True)
    model['smallwin_query_title'] = StandardScaler().fit_transform(model[['smallwin_query_title']])
    model['smallwin_query_desc'] = StandardScaler().fit_transform(model[['smallwin_query_desc']])
    visualize_features(model)
    X = model.loc[:, model.columns != 'relevance']
    y = model.loc[:, model.columns == 'relevance']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

    result = [
        learningmodels.regression(X_train, y_train, X_test, y_test),
        learningmodels.randomforest(X_train, y_train, X_test, y_test),
        learningmodels.ridge(X_train, y_train, X_test, y_test),
        learningmodels.boostregressor(X_train, y_train, X_test, y_test),
        learningmodels.PCAboost(X_train, y_train, X_test, y_test),
        learningmodels.adaboost(X_train, y_train, X_test, y_test)
    ]
    idx += 1
    results.append(result)


df = pd.DataFrame(results, index=model_names).T
df['technique'] = STATISTICAL_MODELS.values()
df.to_csv('results.csv', index=False)

ax = df.plot.bar(x='technique')
ax.set_ylabel("RMSE")
ax.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=2,
            borderaxespad=0, frameon=False)
plt.gcf().subplots_adjust(bottom=0.35)
plt.show()


# Get best result for the different models #


best_results = []
for r in results:
    best_results.append(max(r))


for key, value in STATISTICAL_MODELS.items():
    print(value, ": ", best_results[int(key)])
