import pandas as pd
import re
import unidecode
from sklearn.base import BaseEstimator, TransformerMixin
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

stopWords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()


# Lower String #


def lower_string(string, except_list=None):
    if except_list is None:
        return string.lower()
    else:
        word_tokens = word_tokenize(string)
        s = []
        for word in word_tokens:
            if word in except_list:
                s.append(word)
            else:
                s.append(word.lower())
        return ' '.join(s)


class LowerString(BaseEstimator, TransformerMixin):
    """
    Transformer to lower case a string
    """

    def __init__(self, except_list=None):
        if except_list is None:
            except_list = []
        self.except_list = except_list

    def fit(self, X, y=None):
        return self

    def lower_string(self, string):
        if not self.except_list:
            return string.lower()
        else:
            word_tokens = word_tokenize(string)
            s = []
            for word in word_tokens:
                if word in self.except_list:
                    s.append(word)
                else:
                    s.append(word.lower())
            return ' '.join(s)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.lower_string)
        else:
            return X.apply(self.lower_string)


# Strip class #


def strip_string(string, char=' '):
    return string.strip(char)


class StripString(BaseEstimator, TransformerMixin):
    """
    Transformer to strip a string
    """

    def __init__(self, char=None):
        if char is None:
            char = ' '
        self.char = char

    def fit(self, X, y=None):
        return self

    def strip_string(self, string):
        return string.strip(self.char)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.strip_string)
        else:
            return X.apply(self.strip_string)


# Lemmatizing class #


def lemmatize(string):
    return lemmatizer.lemmatize(string)


class LemmatizeString(BaseEstimator, TransformerMixin):
    """
    Transformer to lemmatize a string
    """

    def fit(self, X, y=None):
        return self

    def lemmatize(self, string):
        return lemmatizer.lemmatize(string)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.lemmatize)
        else:
            return X.apply(self.lemmatize)


# Stemming class


def stemming(string):
    words = string.split()
    ps = PorterStemmer()
    words = list(map(lambda word: ps.stem(word), words))
    return ' '.join(words)


class StemString(BaseEstimator, TransformerMixin):
    """
    Transformer to stem a string
    """

    def fit(self, X, y=None):
        return self

    def stemming(self, string):
        words = word_tokenize(string)
        ps = PorterStemmer()
        words = list(map(lambda word: ps.stem(word), words))
        return ' '.join(words)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.stemming)
        else:
            return X.apply(self.stemming)


# Remove Punctuation


class CleanPunctuation(BaseEstimator, TransformerMixin):
    """
    Transformer to delete punctuation
    """

    def fit(self, X, y=None):
        return self

    def remove_punctuation(self, string):
        regex = r'(?<!\d)[.,;:](?!\d)'
        return re.sub(regex, "", string, 0)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.remove_punctuation)
        else:
            return X.apply(self.remove_punctuation)


def remove_punctuation(string):
    regex = r'(?<!\d)[.,;:](?!\d)'
    return re.sub(regex, "", string, 0)


# Remove Stop Words #


class RemoveStopWords(BaseEstimator, TransformerMixin):
    """
    Transformer to remove stop words
    """

    def fit(self, X, y=None):
        return self

    def remove_stop_words(self, string):
        word_tokens = word_tokenize(string)
        words = [w for w in word_tokens if not w in stopWords]
        return ' '.join(words)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.remove_stop_words)
        else:
            return X.apply(self.remove_stop_words)


def remove_stop_words(string):
    word_tokens = word_tokenize(string)
    words = [w for w in word_tokens if not w in stopWords]
    return ' '.join(words)


# #


class RemoveAccents(BaseEstimator, TransformerMixin):
    """
    Transformer to remove accents
    """

    def fit(self, X, y=None):
        return self

    def remove_accents(self, string):
        return unidecode.unidecode(string)

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.remove_accents)
        else:
            return X.apply(self.remove_accents)


def remove_accents(string):
    return unidecode.unidecode(string)


# #


def tokenize(string):
    return string.split()


def pos_tagger(string):
    return pos_tag(string.split())


# #


class RemoveSpecialChar(BaseEstimator, TransformerMixin):
    """
    Transformer to remove special chars
    """

    def fit(self, X, y=None):
        return self

    def remove_special_char(self, s):
        s = s.replace("$", " ")
        s = s.replace("?", " ")
        s = s.replace("&nbsp;", " ")
        s = s.replace("&amp;", "&")
        s = s.replace("&#39;", "'")
        s = s.replace("/>/Agt/>", "")
        s = s.replace("</a<gt/", "")
        s = s.replace("gt/>", "")
        s = s.replace("/>", "")
        s = s.replace("<br", "")
        s = s.replace("<.+?>", "")
        s = s.replace("[ &<>)(_,;:!?\+^~@#\$]+", " ")
        s = s.replace("'s\\b", "")
        s = s.replace("[']+", "")
        s = s.replace("[\"]+", "")
        s = s.replace("-", " ")
        s = s.replace("+", " ")
        s = s.replace(")", "")
        s = s.replace("(", "")
        s = s.replace("`", "")
        s = s.replace("#", "")
        return s

    def transform(self, X):
        if isinstance(X, pd.DataFrame):
            return X.applymap(self.remove_special_char)
        else:
            return X.apply(self.remove_special_char)


def remove_special_char(s):
    s = s.replace("$", " ")
    s = s.replace("?", " ")
    s = s.replace("&nbsp;", " ")
    s = s.replace("&amp;", "&")
    s = s.replace("&#39;", "'")
    s = s.replace("/>/Agt/>", "")
    s = s.replace("</a<gt/", "")
    s = s.replace("gt/>", "")
    s = s.replace("/>", "")
    s = s.replace("<br", "")
    s = s.replace("<.+?>", "")
    s = s.replace("[ &<>)(_,;:!?\+^~@#\$]+", " ")
    s = s.replace("'s\\b", "")
    s = s.replace("[']+", "")
    s = s.replace("[\"]+", "")
    s = s.replace("-", " ")
    s = s.replace("+", " ")
    s = s.replace(")", "")
    s = s.replace("(", "")
    s = s.replace("`", "")
    s = s.replace("#", "")
    return s
