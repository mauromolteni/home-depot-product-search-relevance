import pandas as pd
import os
import numpy as np

from definitions import DATASET_PATH

################
# Acquire data #
################


def load_data(filename, encodingtype=None):
    os.chdir(DATASET_PATH)
    try:
        if encodingtype is None:
            return pd.read_csv(filename)
        else:
            return pd.read_csv(filename, encoding=encodingtype)
    except:
        print("Error importing data")


def unique_from_list(seq):
    checked = []
    for e in seq:
        if e not in checked:
            checked.append(e)
    return checked


def dataframe_union(a, b):
    return a.join(b)


def join_dataset(a, b, attribute):
    return pd.merge(a, b, how='left', on=attribute)


def handle_miss_values(dataset, replacevalue):
    return dataset.replace(np.nan, replacevalue, regex=True)
