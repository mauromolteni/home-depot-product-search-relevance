import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin


class Selector(BaseEstimator, TransformerMixin):
    """
    Transformer to select a single column from the data frame to perform additional transformations on
    """

    def __init__(self, key):
        self.key = key

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X[self.key]


class ColumnAdder(BaseEstimator, TransformerMixin):
    """
    Transformer to join dataframes
    """

    def __init__(self, dataset, join_attribute):
        self.dataset = dataset
        self.attribute = join_attribute

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return pd.merge(X, self.dataset, how='left', on=self.attribute)

