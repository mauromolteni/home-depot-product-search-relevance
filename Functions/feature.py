from nltk import ngrams
from nltk import FreqDist
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd


def binary_occurrence_matrix(data, ngram_low=1, ngram_hi=2):
    vec = CountVectorizer(ngram_range=(ngram_low, ngram_hi), binary=True)
    features = vec.fit_transform(data)
    df = pd.DataFrame(features.toarray(), columns=vec.get_feature_names())
    return df


def tfidf_matrix(data, ngram_low=1, ngram_hi=2):
    tfidf = TfidfVectorizer(min_df=0.01, max_df=0.95, ngram_range=(ngram_low, ngram_hi))
    features = tfidf.fit_transform(data)
    return pd.DataFrame(
        features.todense(),
        columns=tfidf.get_feature_names()
    )


def get_similarity_from_term_matrix(doc, q):
    return cosine_similarity(doc, q)


def get_similarity(doc, q):
    d_matrix = tfidf_matrix(doc)
    q_matrix = tfidf_matrix(q)
    d_num_of_rows = d_matrix.shape[0]
    frame = [d_matrix, q_matrix]
    result = pd.concat(frame)
    result.reset_index(drop=True, inplace=True)
    result.fillna(0, inplace=True)
    rows = result.iloc[-d_num_of_rows:]
    q_matrix = pd.DataFrame(columns=result.columns)
    q_matrix = q_matrix.append(rows, ignore_index=True)
    d_matrix = result.drop(rows.index)
    del result
    return cosine_similarity(d_matrix, q_matrix)


# Returns a counting term matrix. For each term, the matrix represents how many times it occurs #

def count_term_matrix(data, ngram_low=1, ngram_hi=2):
    vec = CountVectorizer(ngram_range=(ngram_low, ngram_hi))
    features = vec.fit_transform(data)
    df = pd.DataFrame(features.toarray(), columns=vec.get_feature_names())
    return df


# Return 1 if q is in d, otherwise 0. #


def is_q_in_d(q, d):
    if q in d:
        return 1
    return 0


# Takes a string and returns n-gram tokens based on parameter n #

def get_n_gram(string, n):
    s = []
    tokens = [token for token in string.split(" ") if token != ""]
    for ng in list(ngrams(tokens, n)):
        s.append(' '.join(str(i) for i in ng))
    return s


# Takes a string and returns all the n-gram tokens from smallest to largest ones #

def word_grams(words, min=1, max=4):
    s = []
    for n in range(min, max):
        for ng in get_n_gram(words, n):
            s.append(''.join(str(i) for i in ng))
    return s


# Return words in common between str1 and str2 #


def str_common_word(str1, str2):
    words, count = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            count += 1
    return count


# Returns the number of letters in string #


def get_num_of_letters(string):
    return len(string)


# Returns the number of words in string #

def get_num_of_words(string):
    return len(string.split())


# #


def str_whole_word(str1, str2, i_):
    count = 0
    while i_ < len(str2):
        i_ = str2.find(str1, i_)
        if i_ == -1:
            return count
        else:
            count += 1
            i_ += len(str1)
    return count


# Computes the word frequency in string #


def word_frequency(string):
    return FreqDist(string.split())


# Functions that apply to a dataframe for evaluating if a term is in some fields #


def is_query_in_title(dataframe):
    return dataframe.apply(lambda r: is_q_in_d(r['search_term'], r['product_title']), axis=1)


def is_query_in_description(dataframe):
    return dataframe.apply(lambda r: is_q_in_d(r['search_term'], r['product_description']), axis=1)


def is_brand_in_query(dataframe):
    return dataframe.apply(lambda r: is_q_in_d(r['brand'], r['search_term']), axis=1)


def is_material_in_query(dataframe):
    return dataframe.apply(lambda r: is_q_in_d(r['material'], r['search_term']), axis=1)


def get_num_of_words_query(dataframe):
    return dataframe.apply(lambda r: get_num_of_words(r['search_term']), axis=1)


def get_num_of_words_desc(dataframe):
    return dataframe.apply(lambda r: get_num_of_words(r['product_description']), axis=1)


def get_num_of_words_title(dataframe):
    return dataframe.apply(lambda r: get_num_of_words(r['product_title']), axis=1)


def get_num_of_letters_query(dataframe):
    return dataframe.apply(lambda r: get_num_of_letters(r['search_term']), axis=1)


def get_common_words_query_title(dataframe):
    return dataframe.apply(lambda r: str_common_word(r['search_term'], r['product_title']), axis=1)


def get_common_words_query_description(dataframe):
    return dataframe.apply(lambda r: str_common_word(r['search_term'], r['product_description']), axis=1)


def get_smallest_window_query_title(dataframe):
    return dataframe.apply(lambda r: smallest_window(r['search_term'], r['product_title']), axis=1)


def get_smallest_window_query_description(dataframe):
    return dataframe.apply(lambda r: smallest_window(r['search_term'], r['product_description']), axis=1)


# Computes the smallest window width #

def smallest_window(query, doc):
    doc = doc.split()
    query = query.split()
    dict = {}

    # Iterate in the 1st list
    for m in doc:

        # Iterate in the 2nd list
        for n in query:

            # if there is a match
            if m == n:
                if m in dict:
                    dict[m].append(doc.index(m))
                else:
                    dict[m] = [doc.index(m)]

    smallest_win = 2000
    min_index = 2000
    max_index = -2000
    if len(dict) >= len(query):
        for key, value in dict.items():
            if max(value) > max_index:
                max_index = max(value)
            if min(value) < min_index:
                min_index = min(value)
        smallest_win = max_index - min_index + 1

    return smallest_win

