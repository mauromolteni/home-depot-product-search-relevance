import os
from Functions import utils
import definitions


def load_dataset():
    os.chdir(definitions.DATASET_PATH)
    train_data = utils.load_data("train.csv", "Latin-1")
    attributes = utils.load_data("attributes.csv")
    products = utils.load_data("product_descriptions.csv")
    return [train_data, attributes, products]


def enrich_data(data, brand, material, products):
    data = utils.join_dataset(data, brand, 'product_uid')
    data = utils.join_dataset(data, material, 'product_uid')
    data = utils.join_dataset(data, products, 'product_uid')
    return data


def get_attributes(attributes):
    brand = attributes[attributes.name == "MFG Brand Name"][["product_uid", "value"]].rename(columns={"value": "brand"})
    material = attributes[attributes.name == "Material"][["product_uid", "value"]].rename(columns={"value": "material"})
    return brand, material


def clean_dataset(data):
    return utils.handle_miss_values(data, '')
