import pandas as pd
from Pipelines import pipelines
from Functions import utils


# Functions to get different model representation of text #


def feat_eng_first_model(data):
    features = build_feature_dataframe(data)
    return features


def feat_eng_second_model(data):
    features = vector_model_features(data, 1)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_third_model(data):
    features = vector_model_features(data, 2)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_fourth_model(data):
    features = vector_model_features(data, 3)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_fifth_model(data):
    features = vector_model_features(data, 4)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_sixth_model(data):
    features = vector_model_features(data, 5)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_seventh_model(data):
    features = vector_model_features(data, 6)
    return utils.dataframe_union(feat_eng_first_model(data), features)


def feat_eng_eighth_model(data):
    features = count_vector_model_features(data, 2)
    return utils.dataframe_union(feat_eng_first_model(data), features)


# Other functions #


def build_feature_dataframe(data):
    features = pipelines.base_feature_extraction(data)
    features = utils.dataframe_union(features, data[['product_uid', 'relevance']])
    return features


def vector_model_features(data, ngram=1):
    features = pd.DataFrame()
    product_title_matrix, search_term_matrix, brand_matrix, material_matrix, product_des_matrix = \
        vector_model(data, 1, ngram)
    features['similarity_query_title'] = \
        pipelines.advanced_feature_extraction(product_title_matrix, search_term_matrix)
    features['similarity_query_brand'] = \
        pipelines.advanced_feature_extraction(brand_matrix, search_term_matrix)
    features['similarity_query_material'] = \
        pipelines.advanced_feature_extraction(material_matrix, search_term_matrix)
    features['similarity_query_description'] = \
        pipelines.advanced_feature_extraction(product_des_matrix, search_term_matrix)
    return features


def count_vector_model_features(data, ngram=2):
    features = pd.DataFrame()
    product_title_matrix, search_term_matrix, brand_matrix, material_matrix, product_des_matrix = \
        pipelines.counts_binary_model(data, 1, ngram)
    features['similarity_query_title'] = \
        pipelines.advanced_feature_extraction(product_title_matrix, search_term_matrix)
    features['similarity_query_brand'] = \
        pipelines.advanced_feature_extraction(brand_matrix, search_term_matrix)
    features['similarity_query_material'] = \
        pipelines.advanced_feature_extraction(material_matrix, search_term_matrix)
    features['similarity_query_description'] = \
        pipelines.advanced_feature_extraction(product_des_matrix, search_term_matrix)
    return features


def vector_model(data, ngram_low=1, ngram_hi=1):
    return pipelines.vector_model(data, ngram_low, ngram_hi)

