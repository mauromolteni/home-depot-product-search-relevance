from Pipelines.pipelines import normalize_text
from Functions import utils


def process_dataset(data):
    data.drop(['id'], axis=1, inplace=True)
    data['product_uid'] = data['product_uid'].astype(int)
    data['brand'] = utils.handle_miss_values(data[['brand']], '')
    data['material'] = utils.handle_miss_values(data[['material']], '')
    data['brand'] = normalize_text.fit_transform(data[['brand']])
    data['product_title'] = normalize_text.fit_transform(data[['product_title']])
    data['search_term'] = normalize_text.fit_transform(data[['search_term']])
    data['product_description'] = normalize_text.fit_transform(data[['product_description']])
    data['material'] = normalize_text.fit_transform(data[['material']])
    return data

