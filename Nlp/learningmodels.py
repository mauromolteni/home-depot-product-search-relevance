import numpy as np
import pandas as pd
import math
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Ridge
from sklearn.ensemble import GradientBoostingRegressor
from sklearn import linear_model
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.decomposition import PCA
from sklearn.svm import SVR


def regression(X_train, y_train, X_test, y_test):
    lm = linear_model.LinearRegression()
    lm.fit(X_train, y_train.values.ravel())
    y_pred = lm.predict(X_test)
    lm.score(X_train, y_train)
    rf_mse = mean_squared_error(y_pred, y_test)
    rf_rmse = np.sqrt(rf_mse)
    print('Linear Regression RMSE: %.4f' % rf_rmse)
    return rf_rmse


def randomforest(X_train, y_train, X_test, y_test):
    mse = math.inf
    for num_of_est in range(64, 128, 16):
        for max_depth in range(1, 32):
            rf = RandomForestRegressor(n_estimators=num_of_est, max_depth=6, random_state=0)
            rf.fit(X_train, y_train.values.ravel())
            y_pred = rf.predict(X_test)
            rf_mse = mean_squared_error(y_pred, y_test)
            rf_rmse = np.sqrt(rf_mse)
            if rf_rmse < mse:
                mse = rf_rmse
    print('RandomForest RMSE: %.4f' % mse)
    return mse


def ridge(X_train, y_train, X_test, y_test):
    mse = math.inf
    for alpha in np.arange(.1, 1.0, .1):
        rg = Ridge(alpha=alpha)
        rg.fit(X_train, y_train.values.ravel())
        y_pred = rg.predict(X_test)
        rg_mse = mean_squared_error(y_pred, y_test)
        rg_rmse = np.sqrt(rg_mse)
        if rg_rmse < mse:
            mse = rg_rmse
    print('Ridge RMSE: %.4f' % mse)
    return mse


def boostregressor(X_train, y_train, X_test, y_test):
    mse = math.inf
    for n_estimators in range(100, 500, 100):
        for learning_rate in np.arange(0.8, 0.1, -0.1):
            for max_depth in range(1, 8):
                est = GradientBoostingRegressor(n_estimators=n_estimators,
                                                learning_rate=learning_rate,
                                                max_depth=max_depth,
                                                random_state=0, loss='ls')
                est.fit(X_train, y_train.values.ravel())
                y_pred = est.predict(X_test)
                est_mse = mean_squared_error(y_pred, y_test)
                est_rmse = np.sqrt(est_mse)
                if est_rmse < mse:
                    mse = est_rmse
    print('Gradient boosting RMSE: %.4f' % mse)
    return mse


def PCAboost(X_train, y_train, X_test, y_test):
    for ncomp in range(2, X_train.shape[1]-1):
        pca = PCA(n_components=ncomp)
        principalComponents_train = pca.fit_transform(X_train)
        principalComponents_test = pca.fit_transform(X_test)
        X_train = pd.DataFrame(data=principalComponents_train)
        X_test = pd.DataFrame(data=principalComponents_test)
        mse = math.inf
        for n_estimators in range(100, 500, 100):
            for learning_rate in np.arange(0.8, 0.1, -0.1):
                for max_depth in range(1, 8):
                    est = GradientBoostingRegressor(n_estimators=n_estimators,
                                                    learning_rate=learning_rate,
                                                    max_depth=max_depth,
                                                    random_state=0, loss='ls')
                    est.fit(X_train, y_train.values.ravel())
                    y_pred = est.predict(X_test)
                    est_mse = mean_squared_error(y_pred, y_test)
                    est_rmse = np.sqrt(est_mse)
                    if est_rmse < mse:
                        mse = est_rmse
        print('PCA Gradient boosting RMSE: %.4f' % mse)
        return mse


def adaboost(X_train, y_train, X_test, y_test):
    mse = math.inf
    for max_depth in range(2, 6):
        for n_estimators in range(200, 500, 50):
            regr = AdaBoostRegressor(DecisionTreeRegressor(max_depth=max_depth), n_estimators=n_estimators)
            regr.fit(X_train, y_train.values.ravel())
            y_pred = regr.predict(X_test)
            boost_mse = mean_squared_error(y_pred, y_test)
            boost_mse = np.sqrt(boost_mse)
            if boost_mse < mse:
                mse = boost_mse
    print('ADA boosting RMSE: %.4f' % mse)
    return mse


"""
def svm(X_train, y_train, X_test, y_test):
    mse = math.inf
    for kernel in ['rbf', 'poly', 'sigmoid']:
        for c in np.arange(0.7, 1.3, 0.1):
            for epsilon in np.arange(0.1, 0.3, 0.1):
                clf = SVR(kernel=kernel, gamma='scale', C=c, epsilon=epsilon)
                clf.fit(X_train, y_train.values.ravel())
                y_pred = clf.predict(X_test)
                boost_mse = mean_squared_error(y_pred, y_test)
                boost_mse = np.sqrt(boost_mse)
                if boost_mse < mse:
                    mse = boost_mse
    print('SVM RMSE: %.4f' % mse)
    return mse
"""